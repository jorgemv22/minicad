package genaro.ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
//import genaro.objetos.PanelPaint;
import genaro.objetos.Panel;
import genaro.objetos.ColorChooserButton;
import genaro.objetos.Slider;
import genaro.objetos.BotonRotar;

public class Ventana extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//PanelPaint panel_contenedor;
	String objeto;
	Color color_seleccionado = Color.black;
	Panel panel_fondo;
	
	public Ventana()
	{
		super("Diseñador de objetos");
		JPanel panel_principal = new JPanel();
		panel_principal.setLayout(null);
		panel_principal.setBounds(0, 0, 800, 32);
		
		Slider slider = new Slider();
		slider.setBounds(510, 260, 240, 32);
		
		BotonRotar boton_izquierda = new BotonRotar("+");
		boton_izquierda.setBounds(510, 300, 128, 128);
		BotonRotar boton_derecha = new BotonRotar("-");
		boton_derecha.setBounds(630, 300, 128, 128);
		//boton_derecha.setBounds(532, 0, 30, 32);
		
		this.panel_fondo = new Panel(this);
		
		panel_fondo.setBounds(0, 0,  500, 600);
		panel_fondo.setSlider(slider);
		panel_fondo.setRotador(boton_izquierda, "+");
		panel_fondo.setRotador(boton_derecha, "-");
		panel_principal.add(this.panel_fondo);
		
		JButton boton_rectangulo = new JButton();
		boton_rectangulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/cuadrado.jpg")));
		boton_rectangulo.setBounds(510, 20, 110, 110);
		boton_rectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Rectangulo";
				panel_fondo.setObjeto(objeto);
				//panel_contenedor.setColorActual(color_seleccionado);
				//panel_contenedor.setElemento(objeto);
			}
		});
		
		//JButton boton_linea = new JButton();
		//boton_linea.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/linea_32x32.png")));
		//boton_linea.setBounds(32, 0, 32, 32);
		JButton boton_linea = new JButton("-");
		boton_linea.setBounds(630, 20, 110, 110);
		boton_linea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Linea";
				panel_fondo.setObjeto(objeto);
				//panel_contenedor.setColorActual(color_seleccionado);
				//panel_contenedor.setElemento(objeto);
			}
		});
		
		JButton boton_circulo = new JButton();
		boton_circulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/circulo.png")));
		boton_circulo.setBounds(510, 140, 110, 110);
		boton_circulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Circulo";
				panel_fondo.setObjeto(objeto);
				//panel_contenedor.setColorActual(color_seleccionado);
				//panel_contenedor.setElemento(objeto);
			}
		});
		
		JButton boton_triangulo = new JButton();
		boton_triangulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/triangulo.jpg")));
		boton_triangulo.setBounds(630, 140, 110, 110);
		boton_triangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Triangulo";
				panel_fondo.setObjeto(objeto);
				//panel_contenedor.setColorActual(color_seleccionado);
				//panel_contenedor.setElemento(objeto);
			}
		});
		
		/*JButton boton_libre = new JButton();
		boton_libre.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/libre_32x32.png")));
		boton_libre.setBounds(128, 0, 32, 32);
		boton_libre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Libre";
				panel_fondo.setObjeto(objeto);
				//panel_contenedor.setColorActual(color_seleccionado);
				//panel_contenedor.setElemento(objeto);
			}
		});*/
		
		ColorChooserButton ccb = new ColorChooserButton(Color.black);
		//ccb.setBounds(160, 0, 32, 32);
		ccb.setBounds(510, 430, 50, 50);
		ccb.addColorChangedListener(new ColorChooserButton.ColorChangedListener() {
		    @Override
		    public void colorChanged(Color newColor) {
		    	color_seleccionado = newColor;
		    	panel_fondo.setColor(newColor);
		    	//panel_contenedor.setColorActual(color_seleccionado);
		    	System.out.println("Desde Ventana: " + color_seleccionado.toString());
		            // do something with newColor ...
		    }
		});
		
		JLabel lblcolor = new JLabel("Selección de Color");
		lblcolor.setBounds(192, 0, 150, 32);
		
		panel_principal.add(boton_rectangulo);
		panel_principal.add(boton_linea);
		panel_principal.add(boton_circulo);
		panel_principal.add(boton_triangulo);
		//panel_principal.add(boton_libre);
		panel_principal.add(ccb);
		panel_principal.add(lblcolor);
		panel_principal.add(slider);
		panel_principal.add(boton_izquierda);
		panel_principal.add(boton_derecha);
		
		this.add(panel_principal);
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
