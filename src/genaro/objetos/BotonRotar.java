package genaro.objetos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import genaro.eventos.EventosObjetos;

public class BotonRotar extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final ImageObserver Canvas = null;
	private String accion;
	EventosObjetos objeto_seleccionado;
	public int rotacion_angulo = 1;
	public ContenedorFigura panel;
	
	public BotonRotar(String accion)
	{
		this.accion = accion;
		
		if (this.accion == "+")
			this.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/rotar_mas_1.png")));
		else if(this.accion == "-")
			this.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/rotar_menos_1.png")));
		
		this.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				/*int angulo = objeto_seleccionado.getAngulo();
				Graphics g = objeto_seleccionado.getGraficos();*/
				
				int angulo = objeto_seleccionado.getAngulo();
				
				if (accion == "+") {
					angulo = angulo + rotacion_angulo;
					if (angulo >= 360)
						angulo = 0;
				}
				
				if (accion == "-") {
					angulo = angulo - rotacion_angulo;
					if (angulo < 0)
						angulo = rotacion_angulo;
				}
				
				objeto_seleccionado.setAngulo(angulo);
				objeto_seleccionado.Recalcular();
				//panel.setGrados(angulo);
				/*
				
				AffineTransform tx = AffineTransform.getRotateInstance(angulo, 
		                objeto_seleccionado.getWidth()/2, objeto_seleccionado.getHeight()/2);
				
				((Graphics2D) g).setTransform(tx);
				objeto_seleccionado.Actualizar(g);*/
			}
		});
	}
	
	public void setObjeto(EventosObjetos objeto)
	{
		this.objeto_seleccionado = objeto;
		this.panel = objeto_seleccionado.getPanel();
	}
}
